package com.loginsoft.imagetestapp1;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.ArrayList;


public class Gallery extends FragmentActivity {
    private GridView gallery;
    private ArrayList<ImageModel> ImageDataList;
    private ArrayList<ImageModel> ResultDataList;
    private ImageModel ImageData;
    private ImageAdapter mImageAdapter;
    private long ImageMaxNum;
    private Button submitBtn;
    private Intent intent;
    private int Imagecount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        init();
        ImageMaxNum = findImageList();
        Log.e("tag", "Gallery :: ImageMaxNum =" + String.valueOf(ImageMaxNum));
        mImageAdapter = new ImageAdapter(Gallery.this, ImageDataList);
        mImageAdapter.setonImageclickListener(ImageClickListener);
        gallery.setAdapter(mImageAdapter);
        submitBtn.setOnClickListener(submitBtnClickListener);
    }

    private View.OnClickListener submitBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            intent.putExtra("imagedata", ResultDataList);
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private ImageAdapter.onImageclickListener ImageClickListener = new ImageAdapter.onImageclickListener() {
        @Override
        public void onClick(ImageModel data) {
            Log.e("tag", "Gallery :: Selected Image Count =" + Imagecount);
            if (data.getCheckedState()) {
                data.setCheckedState(false);
                for (int i = 0; i < ResultDataList.size(); i++) {
                    if (ResultDataList.get(i).getId().equals(data.getId())) {
                        ResultDataList.remove(data);
                        Log.e("tag", "Gallery :: delete");
                        Imagecount--;
                    }
                }
            } else {
                if (Imagecount < 10) {
                    data.setCheckedState(true);
                    ResultDataList.add(data);
                    Imagecount++;
                    Log.e("tag", "Gallery :: add");
                } else {
                    Toast.makeText(Gallery.this, " 한번에 10장까지만 선택 가능합니다.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void init() {
        gallery = (GridView) findViewById(R.id.gallery);
        ImageDataList = new ArrayList<ImageModel>();
        submitBtn = (Button) findViewById(R.id.submit);
        initImageLoader(Gallery.this);
        ResultDataList = new ArrayList<ImageModel>();
        intent = getIntent();
    }

    private Long findImageList() {
        long returnValue = 0;

        // Select 하고자 하는 컬럼
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA};

        // 쿼리 수행
        Cursor imageCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.DATE_ADDED + " desc ");

        if (imageCursor != null && imageCursor.getCount() > 0) {
            // 컬럼 인덱스
            int imageIDCol = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);
            int imageDataCol = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);

            // 커서에서 이미지의 ID와 경로명을 가져와서 ThumbImageInfo 모델 클래스를 생성해서
            // 리스트에 더해준다.
            while (imageCursor.moveToNext()) {
                ImageData = new ImageModel();
                ImageData.setId(imageCursor.getString(imageIDCol));
                ImageData.setData("file://" + imageCursor.getString(imageDataCol));
                ImageData.setCheckedState(false);
                Log.e("tag", "Gallery :: imagedata =" + ImageData.toString());
                ImageDataList.add(ImageData);
                returnValue++;
            }
        }
        imageCursor.close();
        return returnValue;
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
