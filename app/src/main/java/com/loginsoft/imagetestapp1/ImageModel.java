package com.loginsoft.imagetestapp1;

import java.io.Serializable;

/**
 * Created by Administrator on 2015-08-12.
 */
public class ImageModel implements Serializable{
    private String id;
    private String data;
    private boolean checkedState;

    public void setCheckedState(boolean checkedState) {
        this.checkedState = checkedState;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public boolean getCheckedState(){
        return checkedState;
    }

    @Override
    public String toString(){
        return "id ="+id +"// data ="+data +"// checkedState="+String.valueOf(checkedState);
    }
}
