package com.loginsoft.imagetestapp1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-08-12.
 */
public class ImageAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private DisplayImageOptions options;
    private ArrayList<ImageModel> mImagedataList;
    private ImageLoader mImageLoader;
    private onImageclickListener monImageclickListener;
    private int ImageCount = 0;
    private Context mContext;

    public ImageAdapter(Context context, ArrayList<ImageModel> list) {
        inflater = LayoutInflater.from(context);
        mImagedataList = list;
        mContext = context;
        mImageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        Log.e("tag", "ImageAdapter :: imageListSize =" + getCount());
    }

    @Override
    public int getCount() {
        return mImagedataList.size();
    }

    @Override
    public ImageModel getItem(int i) {
        return mImagedataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.gallery_row, viewGroup, false);
            holder = new ViewHolder();
            holder.Image = (ImageView) view.findViewById(R.id.image);
            holder.checkBox = (ImageView) view.findViewById(R.id.check);
            holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
            holder.Image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) view.getTag();
                    if (mImagedataList.get(position).getCheckedState()){
                        holder.checkBox.setVisibility(View.GONE);
                        ImageCount--;
                    }else {
                        if ( ImageCount <10){
                            holder.checkBox.setVisibility(View.VISIBLE);
                            ImageCount++;
                        }
                    }
                    Log.e("tag", "ImageAdpater :: isClicked!!");
                    monImageclickListener.onClick(getItem(position));
                }
            });
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.Image.setTag(i);
        if (mImagedataList.get(i).getCheckedState()){
            holder.checkBox.setVisibility(View.VISIBLE);
        }else {
            holder.checkBox.setVisibility(View.GONE);
        }
        mImageLoader.displayImage(getItem(i).getData(), holder.Image, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.progressBar.setProgress(0);
                holder.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.progressBar.setVisibility(View.GONE);
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                holder.progressBar.setProgress(Math.round(100.0f * current / total));
            }
        });
        return view;
    }

    public void setonImageclickListener(onImageclickListener listener) {
        monImageclickListener = listener;
    }

    public interface onImageclickListener {
        public void onClick(ImageModel data);
    }

    private class ViewHolder {
        ImageView Image;
        ImageView checkBox;
        ProgressBar progressBar;
    }
}
