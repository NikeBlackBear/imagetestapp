package com.loginsoft.imagetestapp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity {
    private Button testBtn;
    private ImageView testImage;
    private ArrayList<ImageModel> ImageAddreslist;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        testBtn = (Button) findViewById(R.id.testbtn);
        testImage = (ImageView) findViewById(R.id.testImage);
        testBtn.setOnClickListener(testBtnOnClickListener);
    }

    private View.OnClickListener testBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, Gallery.class);
            startActivityForResult(intent, 2);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast.makeText(MainActivity.this, "resultCode : " + resultCode, Toast.LENGTH_SHORT).show();
        if (requestCode == 2 && resultCode == MainActivity.RESULT_OK) {
            ImageAddreslist = (ArrayList<ImageModel>) data.getSerializableExtra("imagedata");
            if (ImageAddreslist != null) {
                for (int i = 0; i < ImageAddreslist.size(); i++) {
                    Log.e("tag", "MainActivity :: Image data " + (i + 1) + "번째 ===" + ImageAddreslist.get(i).toString());
                }
            }
        }
    }
}